import 'package:flutter/material.dart';
import 'package:cams_cloning/theme.dart';

class BoxInfo extends StatelessWidget {
  const BoxInfo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 19, right: 19),
      child: Container(
        height: 47,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.location_on_sharp,
                      size: 20,
                      color: primaryColor,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Jakarta, Indonesia',
                      style: textPrimary,
                    ),
                  ],
                ),
                Text(
                  '09:00 PM',
                  style: textPrimary,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
