import 'package:flutter/material.dart';
import 'package:cams_cloning/theme.dart';

class Header extends StatelessWidget {
  const Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 170,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(24),
          bottomRight: Radius.circular(24),
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            left: -42,
            top: -35,
            child: Container(
              width: 86,
              height: 86,
              decoration: BoxDecoration(
                color: secondaryColor,
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            bottom: 34,
            left: MediaQuery.of(context).size.width - 35,
            child: Container(
              width: 95,
              height: 95,
              decoration: BoxDecoration(
                color: orangeColor,
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            left: 25,
            bottom: 40,
            child: Row(
              children: [
                CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.white,
                  child: CircleAvatar(
                    radius: 35,
                    backgroundImage: AssetImage('images/image.png'),
                  ),
                ),
                SizedBox(
                  width: 12,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kelpin Hartanto',
                      style: nameProfile,
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Junior Programmer - CIS',
                      style: subNameProfile,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: 139,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Divider(
                      thickness: 1,
                      indent: 26,
                      endIndent: 10,
                      color: Colors.white,
                      height: 20,
                    ),
                  ),
                  Text(
                    'Collega.co.id',
                    style: namaPerusahaan,
                  ),
                  Expanded(
                    child: Divider(
                      thickness: 1,
                      indent: 10,
                      endIndent: 26,
                      color: Colors.white,
                      height: 20,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
