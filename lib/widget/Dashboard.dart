import 'package:cams_cloning/theme.dart';
import 'package:cams_cloning/widget/BoxInfo.dart';
import 'package:flutter/material.dart';

import 'Header.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({
    Key key,
  }) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Header(),
        SizedBox(
          height: 35,
        ),
        BoxInfo(),
        SizedBox(
          height: 10,
        ),
        Expanded(
          child: GridView.count(
            padding: EdgeInsets.all(20),
            crossAxisSpacing: 15.0,
            mainAxisSpacing: 15.0,
            crossAxisCount: 2,
            children: [
              GridItem(
                icon: Icons.calendar_today_outlined,
                menu: 'My Activity',
              ),
              GridItem(
                icon: Icons.alarm,
                menu: 'Overtime',
              ),
              GridItem(
                icon: Icons.card_travel,
                menu: 'FWA Schedule',
              ),
              GridItem(
                icon: Icons.directions_run,
                menu: 'On Leave',
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class GridItem extends StatelessWidget {
  final IconData icon;
  final String menu;

  GridItem({this.icon, this.menu});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: [
          Positioned(
            right: -40,
            bottom: -40,
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                color: primaryColor.withOpacity(0.5),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  icon,
                  size: 90,
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  menu,
                  style: textItem.copyWith(fontWeight: FontWeight.bold),
                )
              ],
            ),
          )
        ],
      ),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}
