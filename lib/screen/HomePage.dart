import 'package:cams_cloning/theme.dart';
import 'package:cams_cloning/widget/Dashboard.dart';
import 'package:cams_cloning/widget/Settings.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentab = 0;
  final List<Widget> screens = [
    Dashboard(),
    Setting(),
  ];

  Widget currentScreen = Dashboard();

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6EFEF),
      // body: Dashboard(),
      body: PageStorage(
        bucket: bucket,
        child: currentScreen,
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Absensi',
        onPressed: () {},
        child: Icon(Icons.phone_android),
        backgroundColor: secondaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: primaryColor,
        notchMargin: 10,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    currentScreen = screens[0];
                    currentab = 0;
                  });
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.dashboard,
                      size: 40,
                      color: currentab == 0 ? Colors.white : Color(0xFF6F67B9),
                    ),
                    Text(
                      'Dashboard',
                      style: TextStyle(
                        fontSize: 14,
                        color:
                            currentab == 0 ? Colors.white : Color(0xFF6F67B9),
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    currentScreen = screens[1];
                    currentab = 1;
                  });
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.settings,
                      size: 40,
                      color: currentab == 1 ? Colors.white : Color(0xFF6F67B9),
                    ),
                    Text(
                      'Settings',
                      style: TextStyle(
                        fontSize: 14,
                        color:
                            currentab == 1 ? Colors.white : Color(0xFF6F67B9),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
